﻿namespace Models
{
    public interface IEntity
    {
        public int id { get; set; }
    }
}
