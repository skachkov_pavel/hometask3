﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Models
{
    public class Team : IEntity
    {
        public int id { get; set; }
        public string name { get; set; }
        public DateTime createdAt { get; set; }
    }
}
