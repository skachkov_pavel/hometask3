﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class UserToTasks
    {
        public string UserName { get; set; }
        public List<ProjectTask> ProjectTasks { get; set; }
    }
}
