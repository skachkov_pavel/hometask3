﻿using Models;
using System.Collections.Generic;

namespace HomeTask3.Storage
{
    public interface IRepository<T> where T: IEntity
    {
        void Create(T entity);
        T Read(int id);
        IReadOnlyList<T> ReadAll();
        void Update(T entity);
        void Delete(T entity);
    }
}
