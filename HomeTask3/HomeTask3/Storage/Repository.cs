﻿using Models;
using System.Collections.Generic;
using System.Linq;

namespace HomeTask3.Storage
{
    public class Repository<T> : IRepository<T> where T : IEntity
    {
        private List<T> _entities = new List<T>();

        public void Create(T entity)
        {
            _entities.Add(entity);
        }

        public void Delete(T entity)
        {
            _entities.Remove(entity);
        }

        public T Read(int id)
        {
            return _entities.FirstOrDefault(p => p.id == id);
        }

        public IReadOnlyList<T> ReadAll()
        {
            return _entities.AsReadOnly();
        }

        public void Update(T entity)
        {
            T project = _entities.FirstOrDefault(p => p.id == entity.id);
            if (project != null)
                project = entity;
        }
    }
}
