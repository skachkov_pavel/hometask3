﻿using HomeTask3.Storage;
using Microsoft.AspNetCore.Mvc;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace HomeTask3.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeamsController : ControllerBase
    {
        private readonly IRepository<Team> _repository;

        public TeamsController(IRepository<Team> repository)
        {
            _repository = repository;
        }
        [HttpGet]
        public ActionResult Get()
        {
            return Ok(_repository.ReadAll());
        }

        [HttpGet("{id}")]
        public ActionResult Get(int id)
        {
            return Ok(_repository.Read(id));
        }

        [HttpPost]
        public StatusCodeResult Post([FromBody] Team team)
        {
            _repository.Create(team);
            return new StatusCodeResult(201);
        }

        [HttpPut]
        public StatusCodeResult Put([FromBody] Team team)
        {
            _repository.Update(team);
            return Ok();
        }

        [HttpDelete]
        public StatusCodeResult Delete([FromBody] Team team)
        {
            _repository.Delete(team);
            return Ok();
        }
    }
}
