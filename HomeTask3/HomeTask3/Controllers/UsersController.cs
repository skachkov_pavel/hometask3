﻿using HomeTask3.Storage;
using Microsoft.AspNetCore.Mvc;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace HomeTask3.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IRepository<User> _repository;

        public UsersController(IRepository<User> repository)
        {
            _repository = repository;
        }
        [HttpGet]
        public ActionResult Get()
        {
            return Ok(_repository.ReadAll());
        }

        [HttpGet("{id}")]
        public ActionResult Get(int id)
        {
            return Ok(_repository.Read(id));
        }

        [HttpPost]
        public StatusCodeResult Post([FromBody] User user)
        {
            _repository.Create(user);
            return new StatusCodeResult(201);
        }

        [HttpPut]
        public StatusCodeResult Put([FromBody] User user)
        {
            _repository.Update(user);
            return Ok();
        }

        [HttpDelete]
        public StatusCodeResult Delete([FromBody] User user)
        {
            _repository.Delete(user);
            return Ok();
        }
    }
}
