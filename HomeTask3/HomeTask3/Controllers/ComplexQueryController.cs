﻿using HomeTask3.Storage;
using Microsoft.AspNetCore.Mvc;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace HomeTask3.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ComplexQueryController : ControllerBase
    {
        private readonly IRepository<ProjectTask> _projectTaskRepository;
        private readonly IRepository<User> _userRepository;

        public ComplexQueryController(IRepository<ProjectTask> projectTaskRepository, IRepository<User> userRepository)
        {
            _projectTaskRepository = projectTaskRepository;
            _userRepository = userRepository;
        }


        [HttpGet("GetUserTasksCountPerProject/{id}")]
        public ActionResult GetUserTasksCountPerProject(int id)
        {
            IReadOnlyList<ProjectTask> tasks = _projectTaskRepository.ReadAll();

            return Ok(tasks
                .Where(t => t.performerId == id)
                .GroupBy(t => t.projectId)
                .ToDictionary(g => g.Key, g => g.Count()));
        }

        [HttpGet("GetUserTasksByID/{id}")]
        public ActionResult GetUserTasksByID(int id)
        {
            IReadOnlyList<ProjectTask> allTasks = _projectTaskRepository.ReadAll();
            return Ok(allTasks
                .Where(t => t.performerId == id && t.name.Length < 45)
                .ToList());
          
        }

        [HttpGet("GetUserFinishedTasks/{id}")]
        public ActionResult GetUserFinishedTasks(int id)
        {
            var periodFrom = new DateTime(2021, 1, 1, 0, 0, 0);
            var periodTo = new DateTime(2021, 12, 31, 23, 59, 0);
            IReadOnlyList<ProjectTask> allTasks = _projectTaskRepository.ReadAll();
            return Ok(allTasks
                .Where(t => t.performerId == id && t.finishedAt > periodFrom && t.finishedAt < periodTo)
                .Select(t => new TaskIdToName { Id = t.id, Name = t.name })
                .ToList());
           
        }

        [HttpGet]
        public ActionResult GetSortedUsersWithTasks()
        {
            IReadOnlyList<ProjectTask> allProjectTasks = _projectTaskRepository.ReadAll();
            return Ok(allProjectTasks
                .GroupBy(t => t.performerId)
                .Select(g => new UserToTasks { UserName = _userRepository.Read(g.Key).firstName, ProjectTasks = g.OrderBy(t => t.name.Length).ToList() })
                .OrderBy(o => o.UserName)
                .ToList());
        }


    }
}
