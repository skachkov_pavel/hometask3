﻿using HomeTask3.Storage;
using Microsoft.AspNetCore.Mvc;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace HomeTask3.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectsController : ControllerBase
    {
        private readonly IRepository<Project> _repository;

        public ProjectsController(IRepository<Project> repository)
        {
            _repository = repository;
        }
        [HttpGet]
        public ActionResult Get()
        {
            return Ok(_repository.ReadAll()); 
        }

        [HttpGet("{id}")]
        public ActionResult Get(int id)
        {
            return Ok(_repository.Read(id));
        }

        [HttpPost]
        public StatusCodeResult Post([FromBody] Project project)
        {
            _repository.Create(project);
            return new StatusCodeResult(201);
        }

        [HttpPut]
        public StatusCodeResult Put([FromBody] Project project)
        {
            _repository.Update(project);
            return Ok();
        }

        [HttpDelete]
        public StatusCodeResult Delete([FromBody] Project project)
        {
            _repository.Delete(project);
            return Ok();
        }
    }
}
