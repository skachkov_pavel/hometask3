﻿using HomeTask3.Storage;
using Microsoft.AspNetCore.Mvc;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace HomeTask3.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectTasksController : ControllerBase
    {
        private readonly IRepository<ProjectTask> _repository;

        public ProjectTasksController(IRepository<ProjectTask> repository)
        {
            _repository = repository;
        }
        [HttpGet]
        public ActionResult Get()
        {
            return Ok(_repository.ReadAll());
        }

        [HttpGet("{id}")]
        public ActionResult Get(int id)
        {
            return Ok(_repository.Read(id));
        }

        [HttpPost]
        public StatusCodeResult Post([FromBody] ProjectTask projectTask)
        {
            _repository.Create(projectTask);
            return new StatusCodeResult(201);
        }

        [HttpPut]
        public StatusCodeResult Put([FromBody] ProjectTask projectTask)
        {
            _repository.Update(projectTask);
            return Ok();
        }

        [HttpDelete]
        public StatusCodeResult Delete([FromBody] ProjectTask projectTask)
        {
            _repository.Delete(projectTask);
            return Ok();
        }
    }
}
